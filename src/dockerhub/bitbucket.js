const puppeteer = require('puppeteer')
const logger = require('../log/logger')
const db = require('../db/connect')
const dayjs = require('dayjs')
const { execSync, exec } = require('child_process')
const { promisify } = require('util')
const { chunk } = require('lodash')

const SINGLE_MAX_REPO = 1000
const CREATE_REPO_NUM = 30

const login = async (page) => {
  await page.goto('https://bitbucket.org/', {
    waitUntil: 'domcontentloaded'
  })
  await page.$$eval('a', (list) => {
    return list.find((v) => v.innerHTML == 'Log in').click()
  })
  await page.waitForSelector('#username')
  await page.type('#username', 'xnng77@gmail.com')
  await page.tap('#login-submit')
  await page.waitForResponse('https://id.atlassian.com/rest/marketing-consent/config')
  await page.type('#password', 'xnngs.cn6653')
  await page.tap('#login-submit')
  await page.waitForNavigation()
}

const deleteRepo = async () => {
  try {
    const browser = await puppeteer.launch({
      headless: false,
      defaultViewport: {
        width: 1366,
        height: 768
      },
      args: ['--proxy-server=http://127.0.0.1:1080', '--no-sandbox', '--disable-setuid-sandbox']
    })

    const page = await browser.newPage()

    await login(page)

    const [repoNameList] = await db.query('select * from repo where bind_user_num > 0')

    const handleDeleteRepo = async (repoName) => {
      logger.text(`repoName: ${repoName}`)
      const temPage = await browser.newPage()
      temPage.setDefaultNavigationTimeout(60 * 1000)

      await temPage.goto(`https://bitbucket.org/xnng/${repoName}/admin`)

      await promisify(setTimeout)(3 * 1000)

      const pageTitle = await temPage.evaluate(() => {
        return document.title
      })
      console.log('pageTitle', pageTitle)
      if (pageTitle != '404 — Bitbucket') {
        await temPage.waitForSelector('button[data-testid="repo-settings-details-menu-trigger--trigger"]')

        await temPage.tap('button[data-testid="repo-settings-details-menu-trigger--trigger"]')

        await temPage.waitForTimeout(100)
        const deleteBtnList = await temPage.$$('span[class="Item-z6qfkt-2 eJTYOk"]')
        await deleteBtnList[1].tap()
        await temPage.waitForTimeout(500)
        await temPage.tap('button[data-testid="repo-delete-modal--delete-trigger"]')

        await temPage.waitForNavigation()
        await temPage.close()
      }

      await db.query('delete from repo where repo_name = ?', [repoName])
      await logger.info(`仓库 ${repoName} 删除成功`)
    }

    for (const repoName of repoNameList) {
      try {
        await handleDeleteRepo(repoName.repo_name)
      } catch (error) {
        logger.error(error.stack)
      }
    }
    await browser.close()
  } catch (error) {
    logger.error(error.stack)
  }
}

const createRepo = async () => {
  const browser = await puppeteer.launch({
    headless: false,
    defaultViewport: {
      width: 1366,
      height: 768
    },
    args: ['--proxy-server=http://127.0.0.1:1080', '--no-sandbox', '--disable-setuid-sandbox']
  })

  const page = await browser.newPage()
  page.setDefaultNavigationTimeout(60 * 1000)
  await login(page)

  const handleCreate = async () => {
    const page2 = await browser.newPage()
    page2.setDefaultNavigationTimeout(60 * 1000)

    await page2.goto('https://bitbucket.org/repo/create', {
      waitUntil: 'networkidle0'
    })

    await page2.waitForSelector('#s2id_id_project')
    await page2.tap('#s2id_id_project')
    await page2.waitForResponse('https://bitbucket.org/!api/2.0/teams/xnng/projects/?page=1&pagelen=30&sort=name&q=name~%22%22')
    const orgList = await page2.$$('div[class="select2-result-label project-dropdown--result"]')
    await orgList[0].tap()

    const randomRepoName = Math.random().toString(36).slice(-8)
    await page2.type('#id_name', randomRepoName)
    await page2.tap('label[for="id_is_private"]')
    await page2.tap('button[class="aui-button aui-button-primary"]')

    // await page.waitForTimeout(1000 * 10000)
    await page2.waitForNavigation()
    await page2.close()

    await db.insert('repo', {
      repo_name: randomRepoName,
      username: 'xnng'
    })
    logger.info(`仓库 ${randomRepoName} 创建成功`)
  }

  // eslint-disable-next-line no-unused-vars
  for (const _ of Array.from({ length: CREATE_REPO_NUM })) {
    await handleCreate()
  }
  await browser.close()
}

const pushCode = async () => {
  const [repos] = await db.query('select * from repo')
  const date = dayjs().format('YYYY-MM-DD_HH:mm:ss')
  execSync(`echo ${date} >> temp.md && git add . && git commit -m ${date}`)

  for await (const chunkRepos of chunk(repos, 10)) {
    await Promise.all(
      chunkRepos.map((v) => {
        return new Promise((resolve) => {
          exec(`git push ${v.repo_name} master -f`, (err) => {
            if (!err) {
              logger.text(`提交代码成功，${v.repo_name}`)
              resolve()
            } else {
              if (err.message.includes('not found')) {
                db.query('delete from repo where id = ?', [v.id])
              }
              try {
                execSync(`git remote add ${v.repo_name} https://xnng:WbKegDQGjsEQ2uhPeKrB@bitbucket.org/xnng/${v.repo_name}.git`)
                execSync(`git push ${v.repo_name} master -f`)
              } catch (error) {
                logger.error(`err2:${error.message}`)
              }
              resolve()
            }
          })
        })
      })
    )
  }
}

module.exports = {
  createRepo,
  pushCode,
  deleteRepo
}
