const fs = require('fs')
const path = require('path')
const appRoot = require('app-root-path')
const dayjs = require('dayjs')
const colors = require('colors/safe')

const config = {
  logBasePath: 'log_output',
  map: {
    info: colors.green,
    text: colors.white,
    error: colors.red
  }
}

const logFullPath = path.join(appRoot.path, config.logBasePath)

const logger = {}

for (const [loggerMethod, colorMethod] of Object.entries(config.map)) {
  logger[loggerMethod] = (data) => {
    const dataStr = typeof data == 'string' ? data : JSON.stringify(data)
    const date = dayjs().format('YYYY-MM-DD HH:mm:ss:SSS')
    const fileName = `${dayjs().format('YYYY-MM-DD')}.txt`
    const content = `${String(loggerMethod).toUpperCase()} ${date} 进程：${process.pid}\n${dataStr}`

    console.log(colorMethod(content))
    if (!fs.existsSync(logFullPath)) {
      fs.mkdirSync(logFullPath)
    }

    fs.appendFile(path.join(logFullPath, fileName), content + '\n', (err) => {
      if (err) {
        console.log(`日志写入失败: ${err.message}`)
      }
    })
  }
}

module.exports = logger
